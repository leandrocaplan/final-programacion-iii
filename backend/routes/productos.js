const router = require('express').Router();
let Producto = require('../models/producto.model');

router.route('/').get((req, res) => {
  Producto.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const _id = req.body._id;
  const descripcion = req.body.descripcion;
  const precioCompra = req.body.precioCompra;
  const precioVenta = req.body.precioVenta;
  const stock = req.body.stock;

  const newProducto = new Producto({_id,descripcion,precioCompra,precioVenta,stock});

  newProducto.save()	
    .then(() => res.json('Compra realizada'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Producto.findById(req.params.id)
    .then(producto => res.json(producto))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/actualizarStock/:id').post((req, res) => {

// console.log(req);

	 Producto.findById(req.params.id)
		.then(producto => {
			
			//console.log(JSON.stringify(req.params));
			//console.log(JSON.stringify(req));	 
			//console.log(JSON.stringify(producto));	 
			//producto.stock = Number(producto.stock) + Number(req.body.cantidad);
		  producto.stock = Number(producto.stock) + Number(req.body.cantidad);
		  producto.save()
			.then(() => res.json('Exercise updated!'))
			.catch(err => res.status(400).json('Error: ' + err));
		})
		.catch(err => res.status(400).json('Error: ' + err));

	}

);

module.exports = router;