const router = require('express').Router();

let Venta = require('../models/venta.model');


router.route('/').get((req, res) => {
  Venta.find()
    .then(exercises => res.json(exercises))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
console.log(JSON.stringify(req.body));
  
  const producto = req.body.idProducto;
  const vendedor = req.body.vendedor;
  const cantidad = Number(req.body.cantidad);
  const tipoPago = req.body.tipoPago;
  const total = Number(req.body.total);

  const nuevaVenta = new Venta({
    producto,
	vendedor,
	cantidad,
	tipoPago,
	total
  });
	
	
  nuevaVenta.save()
  .then(() => res.json('Exercise added!'))
  .catch(err => res.status(400).json('Error: ' + err));
  
});

router.route('/:id').get((req, res) => {
  Venta.findById(req.params.id)
    .then(exercise => res.json(exercise))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Venta.findByIdAndDelete(req.params.id)
    .then(() => res.json('Exercise deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
 console.log (JSON.stringify(req.body));
 Venta.findById(req.params.id)
    .then(venta => {
	console.log (JSON.stringify(req.body));		
	venta.producto = req.body.idProducto;
	venta.vendedor = req.body.vendedor;
	venta.cantidad = Number(req.body.cantidad);
	venta.tipoPago = req.body.tipoPago;
	venta.total = Number(req.body.total);	
      
      venta.save()
        .then(() => res.json('Exercise updated!'))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;