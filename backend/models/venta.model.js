const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ventaSchema = new Schema({
  producto: { type: String, required: true },
  vendedor: { type: String, required: true },
  cantidad:	{ type: Number, required: true },
  tipoPago: { type: String, required: true },
  total: { type: Number, required: true }
  
}, {
  timestamps: true,
});

const Venta = mongoose.model('Venta', ventaSchema);

module.exports = Venta;