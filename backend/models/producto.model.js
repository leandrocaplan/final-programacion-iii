const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productoSchema = new Schema({
  _id: { type: String, required: true },
  descripcion: { type: String, required: true },
  precioCompra: { type: Number, required: true },
  precioVenta: { type: Number, required: true },
  stock: { type: Number, required: true },
}, {
  timestamps: true,
});

const Producto = mongoose.model('Producto', productoSchema);

module.exports = Producto;