const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: { type: String,required: true, unique: true, trim: true, minlength: 3},
  nombre: { type: String,required: true, unique: true, trim: true, minlength: 3},
  apellido: { type: String,required: true, unique: true, trim: true, minlength: 3},
  categoria: { type: String,required: true, unique: true, trim: true, minlength: 3}
}, {
  timestamps: true,
});

const User = mongoose.model('User', userSchema);

module.exports = User;