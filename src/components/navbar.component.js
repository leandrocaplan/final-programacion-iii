import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Supermercado Caplan</Link>
        <div className="collpase navbar-collapse">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
			<Link to="/" className="nav-link">Registración de ventas</Link>
          </li>
          <li className="navbar-item">
			<Link to="/create" className="nav-link">Venta</Link>
          </li>
          <li className="navbar-item">
			<Link to="/user" className="nav-link">Alta de empleado</Link>
          </li>
		  <li className="navbar-item">
			<Link to="/nuevoProducto" className="nav-link">Agregar Producto</Link>
          </li>
		  <li className="navbar-item">
			<Link to="/compra" className="nav-link">Comprar Producto</Link>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
}