import React, { Component } from 'react';
import axios from 'axios';

export default class CreateUser extends Component {
  constructor(props) {
    super(props);

    //this.onChangeUsername = this.onChangeUsername.bind(this);
    //this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: 'algo',
	  nombre: 'Leandro',
	  apellido: 'Caplan',
	  categoria: 'vendedor'
    }
  }

  onChangeUsername = (e) => {
    this.setState({
      username: e.target.value
    })
  }
  
  onChangeNombre = (e) => {
    this.setState({
      nombre: e.target.value
    })
  }
  onChangeApellido = (e) => {
    this.setState({
      apellido: e.target.value
    })
  }
  onChangeCategoria = (e) => {
    this.setState({
      categoria: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault();

    const user = {
      username: this.state.username,
	  nombre: this.state.nombre,
	  apellido: this.state.apellido,
	  categoria: this.state.categoria
    }

    console.log(JSON.stringify(user));

    axios.post('http://localhost:5000/users/add', user)
      .then(res => console.log(res.data));

    this.setState({
      username: '',
	  nombre: '',
	  apellido: '',
	  categoria: ''
    })
  }

  render() {
    return (
      <div>
        <h3>Nuevo empleado</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group"> 
            <label>Nombre de ususario: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.username}
                onChange={this.onChangeUsername}
                />
          </div>
		  
		  <div className="form-group"> 
            <label>Nombre: </label>
            <input  type="text"
                required
                className="form-control"
                value={this.state.nombre}
                onChange={this.onChangeNombre}
                />
          </div>
		  
		  <div className="form-group"> 
            <label>Apellido: </label>
            <input type="text"
                required
                className="form-control"
                value={this.state.apellido}
                onChange={this.onChangeApellido}
                />
          </div>
		  
		  <div className="form-group"> 
		   
		   <label>Categoría: </label>

			<select 
				required className="form-control"
				value={this.state.categoria}
				onChange={this.onChangeCategoria}
			>
              

			  <option value="vendedor">Vendedor/a</option>
			  <option value="administrador">Administrador/a</option>
			
			</select>
			
          </div>
		  
          <div className="form-group">
            <input type="submit" value="Crear Usuario" className="btn btn-primary" />
          </div>
        </form>
      </div>
    )
  }
}