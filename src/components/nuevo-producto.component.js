import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class NuevoProducto extends Component {
  constructor(props) {
    super(props);

this.state = {
      producto: '',
      vendedor: '',
	  cantidad: 0,
	  tipoPago: '',
	  total: 0,
	  productos: [],
	  vendedores: []
  }
 }

  onChangeId= (e) => {
    this.setState({
      _id: e.target.value
    })
  }

  onChangeDescripcion = (e) => {
    this.setState({
      descripcion: e.target.value
    })
  }

  onChangePrecioCompra = (e) => {
    this.setState({
      precioCompra: e.target.value
    })
  }

  onChangePrecioVenta = (e) => {
    this.setState({
      precioVenta: e.target.value
    })
  }
  
  onChangeStock = (e) => {
    this.setState({
      stock: e.target.value

    })
  }

  onSubmit = (e) => {
    e.preventDefault();

    const producto = {
	  _id: this.state._id,
	  descripcion: this.state.descripcion,
	  precioCompra: this.state.precioCompra,
	  precioVenta: this.state.precioVenta,
	  stock: this.state.stock
    }

    console.log(producto);

    axios.post('http://localhost:5000/productos/add', producto)
      .then(res => console.log(res.data));


  }

  render() {
    return (
    <div>
      <h3>Información del producto</h3>
      <form onSubmit={this.onSubmit}>
        
		<div className="form-group"> 
          <label>Código: </label>
           <input  type="number"
              required
              className="form-control"
              value={this.state._id}
              onChange={this.onChangeId}
              />
        </div>
		
		
        <div className="form-group"> 
          <label>Descripción: </label>
          <input  type="text"
              required
              className="form-control"
              value={this.state.descripcion}
              onChange={this.onChangeDescripcion}
              />
        </div>
        <div className="form-group">
          <label>Precio de compra: </label>
          <input 
              type="number" 
              className="form-control"
              value={this.state.precioCompra}
              onChange={this.onChangePrecioCompra}
              />
        </div>
        
		
		<div className="form-group">
          <label>Precio de venta: </label>
          <input 
              type="number" 
              className="form-control"
              value={this.state.precioVenta}
              onChange={this.onChangePrecioVenta}
              />
        </div>
        
		
		  <div className="form-group">

          <input 
              type="hidden" 
              className="form-control"
              value={this.state.stock=0}
              onChange={this.onChangeStock}
              />
        </div>

        <div className="form-group">
          <input type="submit" value="Ingresar Producto" className="btn btn-primary" />
        </div>
      </form>
    </div>
    )
  }
}