import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class EditExercise extends Component {
  constructor(props) {
    super(props);


 this.state = {
      producto: '',
      vendedor: '',
	  cantidad: 0,
	  tipoPago: '',
	  total: 0,
	  productos: [],
	  vendedores: []
  }
 }

  
  componentDidMount() {
    axios.get('http://localhost:5000/exercises/'+this.props.match.params.id)
      .then(response => {
        this.setState({
          producto: response.data.producto,
          vendedor: response.data.vendedor,
          cantidad: response.data.cantidad,
		  tipoPago: response.data.tipoPago,
		  total: response.data.total
        })   
      })
      .catch(function (error) {
        console.log(error);
      })
	  
	  axios.get('http://localhost:5000/users/')
      .then(response => {
        if (response.data.length > 0) {
          this.setState({
			  
			vendedores: response.data.map(user => user),
            vendedor: response.data[0]
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })
	  
	  axios.get('http://localhost:5000/productos/')
      .then(response => {
        if (response.data.length > 0) {
          this.setState({
            productos: response.data.map(producto => producto),
            producto: response.data[0]
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })



  }


componentDidUpdate(){
	console.log(this.state.vendedores);
	console.log(this.state.productos);
}

onChangeProducto = (e) => {
    this.setState({
      producto: e.target.value
    })
  }
    
  onChangeVendedor = (e) => {
    this.setState({
      vendedor: e.target.value
    })
  }
  
 onChangeCantidad = (e) => {
    this.setState({
      cantidad: e.target.value
    })
  }
  
  onChangeTipoPago = (e) => {
    this.setState({
      tipoPago: e.target.value
    })
  }
  
  onChangeTotal = (e) => {
    this.setState({
      total: e.target.value
    })
  }  


  onSubmit = (e) => {
    e.preventDefault();

const venta = {
      producto: this.state.producto,
	  vendedor: this.state.vendedor,
	  cantidad: this.state.cantidad,
	  tipoPago: this.state.tipoPago,
	  total: this.state.total,      
    }

    console.log(venta);

    axios.post('http://localhost:5000/exercises/update/' + this.props.match.params.id, venta)
      .then(res => console.log(res.data));


  }

  render() {
    return (
    <div>
      <h3>Registrar venta</h3>
 		
	  <form onSubmit={this.onSubmit}>

		<div className="form-group"> 
          <label>Producto: </label>
          <select ref="otraCosa"
              required
              className="form-control"
              value={this.state.producto}
              onChange={this.onChangeProducto}>
              {
                this.state.productos.map(function(producto) {
                  return <option key={producto._id} value={producto._id}>
						{producto.descripcion}
                    </option>;
                })
              }
          </select>
        </div>
		
		<div className="form-group"> 
          <label>Vendedor: </label>
          <select ref="otraCosa"
              required
              className="form-control"
              value={this.state.vendedor}
              onChange={this.onChangeVendedor}>
              {
                this.state.vendedores.map(function(vendedor) {
                  return <option key={vendedor.username} value={vendedor.username}>
						{vendedor.nombre} {vendedor.apellido}
                    </option>;
                })
              }
          </select>
        
		<div className="form-group">
          <label>Cantidad: </label>
          <input 
              type="number" 
              className="form-control"
              value={this.state.cantidad}
              onChange={this.onChangeCantidad}
              />
        </div>
		
		<div className="form-group"> 
		   
		   <label>Tipo de pago: </label>

			<select 
				required className="form-control"
				value={this.state.tipoPago}
				onChange={this.onChangeTipoPago}
			>
              
			  <option value="otro">Otro</option>
			  <option value="efectivo">Efectivo</option>
			  <option value="credito">Tarjeta de crédito</option>
			  <option value="debito">Tarjeta de débito</option>
			
			</select>
			
          </div>
		  
		<div className="form-group">
          <label>Total: </label>
               <input 
              type="hidden" 
              className="form-control"
              value={this.state.total = this.state.producto.precioVenta * this.state.cantidad}
              onChange={this.onChangeTotal}
              />
		   {' $ ' + this.state.producto.precioVenta * this.state.cantidad}
        </div>
		
	</div>

        <div className="form-group">
          <input type="submit" value="Edit Exercise Log" className="btn btn-primary" />
        </div>
      </form>
    </div>
    )
  }
}