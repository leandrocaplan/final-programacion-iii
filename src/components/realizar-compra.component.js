import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class ComprarProducto extends Component {
  constructor(props) {
    super(props);

    this.state = {
      producto: '',
	  cantidad: 0,
	  monto: 0,
	  productos: []
	  
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/productos/')
      .then(response => {
        if (response.data.length > 0) {
          this.setState({
            productos: response.data.map(producto => producto),
            producto: response.data[0]
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })

  }

  onChangeProducto = (e) => {
    this.setState({
      producto: e.target.value
    })
  }

  onChangeCantidad = (e) => {
    this.setState({
      cantidad: e.target.value
	 
    })
  }

  onChangeMonto = (e) => {
    this.setState({
      monto: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault();
	
	
    const compra = {
      producto: this.state.producto._id,
      cantidad: this.state.cantidad,
      monto: this.state.monto
    }

    console.log(compra);

    axios.post('http://localhost:5000/productos/actualizarStock/' + this.state.producto._id, compra)
      .then(res => console.log(res.data));
  }

  render() {
    return (
    <div>
      <h3>Información de compra</h3>
      <form onSubmit={this.onSubmit}>
        <div className="form-group"> 
          <label>Producto: </label>
          <select ref="otraCosa"
              required
              className="form-control"
              value={this.state.producto}
              onChange={this.onChangeProducto}>
              {
                this.state.productos.map(function(producto) {
                  return <option 
                    key={producto._id} value={producto._id}>
						{producto.descripcion}
                    </option>;
                })
              }
          </select>
        </div>
		
        <div className="form-group">
          <label>Cantidad: </label>
          <input 
              type="number" 
              className="form-control"
              value={this.state.cantidad}
              onChange={this.onChangeCantidad}
              />
        </div>

		<div className="form-group">
          <label>Total: </label>
               <input 
              type="hidden" 
              className="form-control"
              value={this.state.total = this.state.producto.precioCompra * this.state.cantidad}
              onChange={this.onChangeTotal}
              />
		   {' $ ' + this.state.producto.precioCompra * this.state.cantidad}
        </div>
        
		
		<div className="form-group">
          <input type="submit" value="Create Exercise Log" className="btn btn-primary" />
        </div>
      </form>
    </div>
    )
  }
}