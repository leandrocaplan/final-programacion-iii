import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Venta = props => (
  <tr>
    <td>{props.venta.producto}</td>
    <td>{props.venta.vendedor}</td>
    <td>{props.venta.cantidad}</td>
	<td>{props.venta.tipoPago}</td>
	<td>{props.venta.total}</td>
	<td>
      <Link to={"/edit/"+props.venta._id}>edit</Link> | <a href="#" onClick={() => { props.deleteExercise(props.venta._id) }}>delete</a>
    </td>   
  </tr>
)

export default class ExercisesList extends Component {
  constructor(props) {
    super(props);


    this.state = {ventas: []};
  }

  componentDidMount(){
    axios.get('http://localhost:5000/exercises/')
      .then(response => {
        this.setState({ ventas: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteExercise = (id) => {
    axios.delete('http://localhost:5000/exercises/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      exercises: this.state.exercises.filter(el => el._id !== id)
    })
  }

  exerciseList() {
    return this.state.ventas.map(currentexercise => {
      return <Venta venta={currentexercise} deleteExercise={this.deleteExercise} key={currentexercise._id}/>;
    })
  }

  render() {
    return (
      <div>
        <h3>Ventas registradas</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Producto</th>
              <th>Vendedor</th>
              <th>Cantidad</th>
              <th>Tipo de Pago</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            { this.exerciseList() }
          </tbody>
        </table>
      </div>
    )
  }
}